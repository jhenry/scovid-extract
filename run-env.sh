#!/bin/sh

# Need to be in the directory when activating pipenv/virtualenv
pushd /home/bbuser/bin/vosha-export

# Activate python shell
source scl_source enable rh-python38

# Run via pipenv
/home/bbuser/.local/bin/pipenv run python3 /home/bbuser/bin/vosha-export/get-column.py

popd

