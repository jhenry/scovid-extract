import logging
import logging.handlers
import urllib.parse as urlparse
import time
import os
import cx_Oracle
from sh import tail
import smtplib
from email.message import EmailMessage
from shutil import copyfile
from urllib.parse import parse_qs
import csv
from bbrest.bbrest import BbRest
import config

COURSE_ID = config.settings['course_id']
DEBUG = config.settings['debug']
RESULTS_OUTPUT = config.settings['output_path']
ENROLLMENT_REQUEST_LIMIT = config.settings['max_enrollment_requests']
GRADEBOOK_REQUEST_LIMIT = config.settings['max_gradebook_requests']
LOG_PATH = config.settings['log_path']
ARCHIVE_PATH = config.settings['archive_path']
ADMIN_EMAIL = config.settings['admin_email']
LOG_EMAIL = config.settings['log_email']
NOTIFICATIONS = config.settings['notify']

def setupLoggers():
  logger = logging.getLogger(__name__)

  # Create handlers
  consoleHandler = logging.StreamHandler()
  fileHandler = logging.handlers.TimedRotatingFileHandler(LOG_PATH, when="d", interval=1,  backupCount=5)
  consoleHandler.setLevel(logging.DEBUG)
  fileHandler.setLevel(logging.DEBUG)

  # Create formatters and add to handlers
  consoleFormat = fileFormat = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
  consoleHandler.setFormatter(consoleFormat)
  fileHandler.setFormatter(fileFormat)

  # Add handlers to the logger
  logger.addHandler(consoleHandler)
  logger.addHandler(fileHandler)

  if DEBUG:
    logger.setLevel(logging.DEBUG)
  else:
    logger.setLevel(logging.INFO)
  
  return logger

logger = setupLoggers()

"""
Returns BbRest object for interacting with remote application.
"""
def getBbRest():
  bb = BbRest(config.app['key'], config.app['secret'], config.app['url'])
  return bb

def getExternalGradeColumn(columns):
  if 'results' in columns:
    for column in columns['results']:
      if 'externalGrade' in column:
        if column['externalGrade']:
          return column
  else:
      logger.error("Missing/Inaccessible external column. " + str(columns))

def getExternalColumnId():
    bb = getBbRest()
    columns = bb.GetGradeColumns(courseId=COURSE_ID)
    externalGradeColumn = getExternalGradeColumn(columns.json()) 
    return externalGradeColumn['id']

def getRoster(courseIdentifier):
  bb = getBbRest()
  response = bb.GetCourseMemberships(limit=ENROLLMENT_REQUEST_LIMIT, courseId=courseIdentifier,params={'expand':'user', 'fields':'userId,courseId,courseRoleId,user.externalId, availability.available', 'availability.available':'Yes'})
  return response

def BbIdToPk(id):
    parts = id.split("_")
    return parts[1]

def getGradesFromDb():
    externalColumnId = getExternalColumnId()
    columnPk = BbIdToPk(externalColumnId)
    grades = dict()
    cx_Oracle.init_oracle_client(config_dir=config.db['tns_admin'])
    connection = cx_Oracle.connect(config.db['user'], config.db['pass'], config.db['instance'])

    query = "select users.batch_uid, highest_attempt_pk1, manual_score from gradebook_grade, course_users, users where gradebook_grade.course_users_pk1 = course_users.pk1 and course_users.users_pk1 = users.pk1 AND gradebook_main_pk1 = " + columnPk 
    cursor = connection.cursor()
    cursor.execute(query)
    for batch_uid, highest_attempt_pk1, manual_score in cursor.fetchall():
        attemptScore = None
        studentGrade = dict()
        grade = ''

        if highest_attempt_pk1:
            scoreQuery = "select score from attempt where pk1 = " + str(highest_attempt_pk1) 
            for score in cursor.execute(scoreQuery):
                attemptScore = score[0]

        if manual_score:
            if manual_score > 0:
                grade = manual_score
        else:
            grade = attemptScore
            
        grades[batch_uid] = grade

    return grades
        
def buildScoreSheet(scores,roster):
    scoreSheet = list()
    passing = 0
    for member in roster['results']:
      externalId = member['user']['externalId']
    
      if externalId in scores:
        score = scores[externalId]
      else:
        score = -1
      if member['courseRoleId'] == 'Student':
        row = dict()
        completion = scoreToCompletion(score)
        row['SPRIDEN_ID'] = externalId
        row['COMPLETION_IND'] = completion
        scoreSheet.append(row)
        if completion == 'VC':
            passing += 1
    logger.info(str(passing) + " total passing (VC) attempts.")
    return scoreSheet

def scoreToCompletion(score):
    completion = ''
    if score:
        if score >= 100:
            completion = 'VC'
    return completion

def writeGrades(scores):
  with open(RESULTS_OUTPUT, 'w', newline='') as csvfile:
        fieldnames = ['SPRIDEN_ID', 'COMPLETION_IND']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='|')
        writer.writeheader()
        for score in scores:
          writer.writerow(score)
  archiveFile(RESULTS_OUTPUT)

"""
Copy files to an archive/history directory and datestamp them.
"""
def archiveFile(file):
  fileName = os.path.basename(file)
  timeStamp = time.strftime("%Y%m%d_%H%M")
  stampedFileName = timeStamp + "_" + fileName
  archivedFilePath = ARCHIVE_PATH + '/' + stampedFileName
  copyfile(file, archivedFilePath)

def callsLeft():
  if DEBUG:
    bb = getBbRest()
    logger.debug(bb.calls_remaining())

def mailLogs():
    if NOTIFICATIONS:
        log = tail(LOG_PATH)
        msg = EmailMessage()
        msg.set_content(str(log))
        timeStamp = time.strftime("%Y-%m-%d %H:%M")
        msg['Subject'] = "SVOSHA extract, " + timeStamp
        msg['From'] = ADMIN_EMAIL
        msg['To'] = LOG_EMAIL
        s = smtplib.SMTP('localhost')
        s.send_message(msg)
        s.quit()

    

def runExport():
    logger.info("===== Starting Extract =====")
    logger.info("Getting roster...")
    roster = getRoster(COURSE_ID)
    rosterJson = roster.json()

    rosterSize = len(rosterJson['results']) 
    logger.info("Roster retrieved, with " + str(rosterSize) + " records.  Getting grades...")
    grades = getGradesFromDb()

    gradesSize = len(grades) 
    logger.info("Grades retrieved, with " + str(gradesSize) + " attempts/entries.  Formatting and writing output...")
    scoreSheet = buildScoreSheet(grades, rosterJson)

    logger.info("Writing output...")
    writeGrades(scoreSheet)

    logger.info("Extract complete.")

    mailLogs()

runExport()

# vim: set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab syntax=python:
